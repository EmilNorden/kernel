#include "screen.h"

#include "../kernel/util.h"

#define VIDEO_ADDRESS	0xb8000
#define MAX_ROWS	25
#define MAX_COLS	80

#define WHITE_ON_BLACK	0x0f

#define REG_SCREEN_CTRL	0x3d4
#define REG_SCREEN_DATA	0x3d5

char selected_text_attribute = WHITE_ON_BLACK;

int get_cursor(void);
int get_screen_offset(int col, int row);
int handle_scrolling(int cursor_offset);
void set_cursor(int offset);

void clear_screen() {
	int row = 0;
	int col = 0;

	for(row = 0; row < MAX_ROWS; ++row) {
		for(col = 0; col < MAX_COLS; ++col) {
			print_char(' ', col, row, selected_text_attribute);
		}
	}

	set_cursor(get_screen_offset(0, 0));
}

int get_cursor(void) {
	// The device uses its control register as an index
	// to select its internal registers, of which we are interested in:
	// 	reg 14: which is the high byte of the cursor's offset
	//	reg 15: which is the low byte of the cursor's offset
	// Once the internal register has been selected, we may read or write a byte on the data register
	port_byte_out(REG_SCREEN_CTRL, 14);
	int offset = port_byte_in(REG_SCREEN_DATA) << 8;
	port_byte_out(REG_SCREEN_CTRL, 15);
	offset += port_byte_in(REG_SCREEN_DATA);

	// Since the cursor offset reported by the VGA hardware is the number of characters,
	// we multiply it by two to convert it to a character cell offset
	return offset * 2;
}

int get_screen_offset(int col, int row) {
	//return row * (MAX_COLS * 2) + (col*2);
	return (row * MAX_COLS + col) * 2;
}

int handle_scrolling(int cursor_offset) {

	unsigned char *vidmem = (unsigned char*) VIDEO_ADDRESS;

	// If the cursor is within the screen, return it unmodified
	if(cursor_offset < MAX_ROWS * MAX_COLS * 2) {
		return cursor_offset;
	}

	// Shuffle the rows back one
	int i;
	for(i = 1; i < MAX_ROWS; ++i) {
		memory_copy(	&vidmem[get_screen_offset(0, i)],
				&vidmem[get_screen_offset(0, i-1)],
				MAX_COLS * 2
		);
	}

	// Blank the last line by setting all bytes to 0
	char *last_line = &vidmem[get_screen_offset(0, MAX_ROWS-1)];
	for(i = 0; i < MAX_COLS * 2; i += 2) {
		last_line[i] = 0;
	}

	// Move the offset back one row, such that it is now on the last 
	// row, rather than off the edge of the screen.
	cursor_offset -= 2*MAX_COLS;
	
	// return the updated cursor position
	return cursor_offset;
}

void print(const char *message) {
	print_at(message, -1, -1);
}

void print_at(const char *message, int col, int row) {
	// Update the cursor if col and row not negative
	if(col >= 0 && row >= 0) {
		set_cursor(get_screen_offset(col, row));
	}

	int i = 0;
	while(message[i] != 0) {
		print_char(message[i++], col++, row, 0);
	}
}

void print_char(char character, int col, int row, char attribute_byte) {

	unsigned char *vidmem = (unsigned char*) VIDEO_ADDRESS;

	if(!attribute_byte) {
		attribute_byte = selected_text_attribute;
	}

	int offset;
	if(col >= 0 && row >= 0) {
		offset = get_screen_offset(col, row);
	}
	else {
		offset = get_cursor();
	}

	if(character == '\n') {
		int rows = offset / (2*MAX_COLS);
		offset = get_screen_offset(MAX_COLS - 1, rows);
	}
	else {
		vidmem[offset] = character;
		vidmem[offset+1] = attribute_byte;
	}

	offset += 2;
	offset = handle_scrolling(offset);

	set_cursor(offset);
}

void set_cursor(int offset) {
	offset /= 2; // Convert from cell offset to char offset
	// This is similar to get_cursor, only now we write
	// bytes to those internal device registers
	port_byte_out(REG_SCREEN_CTRL, 14);
	port_byte_out(REG_SCREEN_DATA, (unsigned char)(offset >> 8));
	port_byte_out(REG_SCREEN_CTRL, 15);
	port_byte_out(REG_SCREEN_DATA, (unsigned char)offset);
}

void set_text_attribute(char attribute_byte) {
	selected_text_attribute = attribute_byte;
}

void clear_text_attribute() {
	selected_text_attribute = selected_text_attribute;
}
