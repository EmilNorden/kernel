; Ensures that we jump straight into the kernel's entry function.
[bits 32]	; We're in protected mode by now, so use 32-bit instructions
[extern main]	; Declare that we will be referencing the external symbol 'main'
		; so the linker can substitute the final address

mov ebx, TEST_MSG
call print_string_pm

call main	; invoke main() in our C kernel
jmp $		; Hang forever when we return from the kernel

%include "boot/print_string_pm.asm"

TEST_MSG: db "Is this thing on?", 0
