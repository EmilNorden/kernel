#include "../drivers/screen.h"

void main() {
	/* Create a pointer to a char, and point it to the first text cell of video memory
	 (i.e. the top-leftof the screen)*/
	char *video_memory = (char*)0xb8000;
	/* At the address pointed to by video_memory, store the character 'X'
	 (i.e. display 'X' in the top-left of the screen).
	 *video_memory = 'X'; */
	char attribute_byte = 0x0F;
	set_text_attribute(attribute_byte);	
	clear_screen();
	print("Hello, world!\nThis is Emils kernel!\n");
	//print("Testing.....\n");
}
