[bits 16]

; Switch to protected mode
switch_to_pm:

	cli	; We must switch of interrupts until we have set-up
		; the protected mode interrupt vector
		; otherwise interrupts will run riot.

	lgdt [gdt_descriptor]	; Load our GDT

	; Set the first bit of cr0, to make switch to protected mode
	; It can not be set directly, so use general purpose register in between.
	mov eax, cr0
	or eax, 0x1
	mov cr0, eax


	jmp CODE_SEG:start_protected_mode	; Make far jump (i.e. to a new segment) to our 32-bit
						; code. This also forces the CPU to flush its cache of
						; pre-fetched and real-mode decoded instructions, which
						; can cause problems

[bits 32]

; Initialise registers and the stack once in PM.
start_protected_mode:
	; Now we are in 32-bit protected mode.

	mov ax, DATA_SEG
	mov ds, ax
	mov ss, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	mov ebp, 0x90000	; Update our stack position so it is right
	mov esp, ebp		; at the top of the free space
	
	call BEGIN_PM
