#ifndef DRIVER_SCREEN_H_
#define DRIVER_SCREEN_H_

void clear_screen(void);
void print(const char *message);
void print_at(const char *message, int col, int row);
void print_char(char character, int col, int row, char attribute_byte);

void set_text_attribute(char attribute_byte);
void clear_text_attribute();

#endif
