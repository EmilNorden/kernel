[bits 32]
; Define some constants
VIDEO_MEMORY equ 0xb8000
WHITE_ON_BLACK equ 0x0f

; prints a null-terminated string pointed to by EDX
print_string_pm:
	pusha
	mov edx, VIDEO_MEMORY	; Set EDX to start of video memory.

print_string_pm_loop:
	mov al, [ebx]		; Store the char at EBX in AL
	mov ah, WHITE_ON_BLACK	; Store the attributes in AH

	cmp al, 0		; if (AL == 0), end of string, so
	je print_string_pm_done	; jump to done

	mov [edx], ax		; Store char and attributes at current char. cell
	
	add ebx, 1		; Increment to next char
	add edx, 2		; Increment video memory pointer

	jmp print_string_pm_loop

print_string_pm_done:
	popa
	ret 
