
; print_string works by taking the argument passed in bx and printing each character until it reaches a null-byte.
print_string:
pusha

mov ah, 0x0e 	; tele-type output

print_string_loop:
mov dx, [bx]
cmp dl, 0
je print_string_finish
mov al, dl
int 0x10
add bx, 1
jmp print_string_loop

print_string_finish:

popa
ret

; print_hex works by taking the argument passed in dx and :
; examining the value of the bottom 4 bits (4bit values can range from 0-15) and converting that to corresponding character in hex notation
; that character is then placed into the template string in HEX_OUT, at the position denoted by the value in bx.
; bx is then decremented to the next position in the string, and the argument is right-shifted 4 bits and the process is done again.
; After 4 iterations, the string at HEX_OUT will represent the value passed in dx. print_string is used to print the string.
print_hex:
pusha

mov ax, 4		; loop counter

mov bx, HEX_OUT 	; Set bx to base address of template string
add bx, 5		; Add offset of 5 to point at the last character

hex_loop:

mov cx, dx		; move the value into cx
and cx, 0xf 		; AND with 15, all lower 4 bits set

cmp cx, 0xa		; Compare with 0xa (10)
jl set_less_10		; if lower than 10, jump to set_less_10
add cx, 0x57		; else add offset 0x57 to cx (0x61 is 'a' in ascii table, so if cx is 10(0xa) then 0x57+0xa=0x61)
jmp end_set
set_less_10:
add cx, 0x30		; 0x30 is 0 on the ascii table, so add that as an offset into cx

end_set:

mov [bx], cl		; Move the ascii character in cx to where bx is pointing

sub ax, 1 		; decrement loop counter
cmp ax, 0
je done			; if ax == 0, jump to done
			; else
shr dx, 4		; shift dx right 4 bits
sub bx, 1		; move to the left in the hex string

jmp hex_loop
done:
mov bx, HEX_OUT
call print_string

popa
ret

; Global variables
HEX_OUT: db '0x0000', 0
